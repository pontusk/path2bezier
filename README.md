# Path ➡ Bezier app

This app was made to provide an easy way to convert a SVG path string into a format usable with [GreenSock's BezierPlugin](https://greensock.com/BezierPlugin-JS). GreenSock has a nice way of doing this using `MorphSVGPlugin.pathDataToBezier("#path")`, but you can't use that unless you are a paying member. Although GreenSock's premium plugins are fantastic and well worth paying for, you may want to use the free BezierPlugin with your own vector paths even if you don't plan on using MorphSVGPlugin.

---

* Inspired by Balaj Marius's [svg2jsx app](http://svg2jsx.herokuapp.com/).

* The solutions used were found in [this thread on the GreenSock forum](https://greensock.com/forums/topic/10688-challenge-convert-svg-path-to-bezier-anchor-and-control-points/). I looked mostly at [this pen by GreenSock's official CodePen account](https://codepen.io/GreenSock/pen/ecdfb83c70724638f83376a0cfad6b26). Paths are converted to cubic beziers using [Snap.svg](http://snapsvg.io/), then converted into the proper format with regular JavaScript.

* I took that solution and made it into a simple React app using [Create React App](https://github.com/facebook/create-react-app).

* Just paste a path string into one field and copy the bezier points array from the other field.