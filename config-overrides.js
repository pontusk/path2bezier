const {
  override,
  useEslintRc
} = require('customize-cra');

/* eslint-disable */
/* config-overrides.js */
module.exports = override(useEslintRc());
/* eslint-enable */
