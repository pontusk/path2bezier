import React, { Fragment, useState, useEffect, useRef } from 'react';
import './App.css';
import convertToBezier from './convertToBezier';
import { usePrevious } from './hooks';
import GitLab from './GitLab';

// console.log(convertToBezier('M0,0c0,0,35,240,142,240s107,0,107,0s94-7,85-82S299,34,233,71s-80.393,81-65.696,120S159,281,239,309s80,28,80,28s182,29,195-62s74-184,0-190s-134-30-97,61s208,107,230,40s22-67,22-67l0,0L343,270l263,76l96-220c0,0,106,33,71,148s-35,115-35,115'));

const App = () => {
  const [pathString, setPathString] = useState();
  const [bezierObject, setBezierObject] = useState();
  const [copySuccess, setCopySuccess] = useState();
  const prevPathString = usePrevious();
  const ref = useRef();

  useEffect(() => {
    if (prevPathString !== pathString) {
      const convertedPath = convertToBezier(pathString);
      setBezierObject(convertedPath);
    }
  }, [pathString, prevPathString]);

  const copyToClipboard = e => {
    ref.current.select();
    document.execCommand('copy');
    e.target.focus();
    setCopySuccess('Copied to clipboard!');
  };
  return (
    <Fragment>
      <header>
        <h1>Path➡Bezier</h1>
        <GitLab />
      </header>
      <form>
        <label htmlFor="path-string" id="path-string-label">
          Path String{' '}
          <span className="explanation">
            (Found in the SVG path&#39;s &quot;d&quot; attribute)
          </span>
        </label>
        <textarea
          name="path-string"
          id="path-string"
          placeholder="Paste your path string here."
          className="form-control"
          value={pathString}
          onChange={e => setPathString(e.target.value)}
        />
        <label htmlFor="bezier-object" id="bezier-object-label">
          Bezier Points Array
        </label>
        {document.queryCommandSupported('copy') && (
          <div className="copy-button">
            <button onClick={copyToClipboard}>Copy</button>
            {copySuccess && <span className="copy-success">{copySuccess}</span>}
          </div>
        )}
        <textarea
          ref={ref}
          name="bezier-object"
          id="bezier-object"
          placeholder="Copy resulting bezier points from here."
          className="form-control"
          value={pathString && JSON.stringify(bezierObject)}
        />
      </form>
    </Fragment>
  );
};
export default App;
