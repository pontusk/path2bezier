import React from 'react';

const GitLab = () => (
  <a
    href="https://gitlab.com/pontusk/path2bezier"
    title="GitLab"
    target="_blank"
    rel="noopener noreferrer"
    alt="GitLab"
    className="gitlab"
  >
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="6 6 36 36">
      <path
        fill="#FFF"
        d="M6 10a4 4 0 0 1 4-4h28a4 4 0 0 1 4 4v28a4 4 0 0 1-4 4H10a4 4 0 0 1-4-4V10z"
      />
      <defs>
        <filter id="a" filterUnits="userSpaceOnUse" x="6" y="6" width="36" height="36">
          <feColorMatrix
            values="-1 0 0 0 1 0 -1 0 0 1 0 0 -1 0 1 0 0 0 1 0"
            colorInterpolationFilters="sRGB"
            result="source"
          />
          <feFlood result="back" floodColor="#fff" floodOpacity="1" />
          <feBlend in="source" in2="back" />
        </filter>
      </defs>
      <mask maskUnits="userSpaceOnUse" x="6" y="6" width="36" height="36" id="b">
        <g filter="url(#a)">
          <path
            fill="#D6D6D6"
            d="M39.521 26.06l-1.743-5.365-3.456-10.634a.593.593 0 0 0-1.129 0l-3.456 10.634H18.263l-3.455-10.634c-.178-.548-.952-.548-1.13 0l-3.456 10.634L8.48 26.06a1.188 1.188 0 0 0 .431 1.327L24 38.351l15.088-10.964c.417-.302.591-.838.433-1.327z"
          />
          <g fill="#BCBCBC">
            <path d="M10.223 20.694L8.48 26.06a1.188 1.188 0 0 0 .431 1.327L24 38.351l-5.737-17.656h-8.04zM39.521 26.06l-1.743-5.365h-8.041L24 38.351l15.088-10.964c.417-.302.591-.838.433-1.327z" />
          </g>
          <path
            fill="#999"
            d="M24 38.351l-5.737-17.656h-8.041L24 38.351zM24 38.351l5.736-17.656h8.041L24 38.351z"
          />
        </g>
      </mask>
      <path
        mask="url(#b)"
        id="gitlab"
        d="M6 10a4 4 0 0 1 4-4h28a4 4 0 0 1 4 4v28a4 4 0 0 1-4 4H10a4 4 0 0 1-4-4V10z"
      />
    </svg>
  </a>
);

export { GitLab as default };
